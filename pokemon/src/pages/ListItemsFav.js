import "./ListItemsFav.css";
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from "@fortawesome/free-regular-svg-icons";
import axios from "axios";
import { navigate } from "@reach/router";

const pokemonPage = (item) => {
  var str = item;
  localStorage.setItem("pokemonAtual", str);
  navigate("./pokemon");
  window.location.reload(false);
};

function ListItemsFav(props) {
  var usuario = localStorage.getItem("usuarioAtual");
  console.log("esse é o usuario atual");
  console.log(usuario);
  const items = props.items;
  console.log(props.items);
  const listItemsFav = items.map((item) => {
    return (
      <div className="pokeDisplay" key={item.id}>
        <div
          className="cards2"
        >
          <img
            src={item.image_url}
            className="foto_pokemon"
            alt="Pokeimage"
            onClick={(e) => {
              pokemonPage(item.name);
            }}
          ></img>
          <p className="numero">{item.number}</p>
          <FontAwesomeIcon
            className="faicons"
            onClick={() => {
              axios
                .post(
                  `https://pokedex20201.herokuapp.com//users/${usuario}/starred/${item.name}`
                )
                .then((res) => {
                  console.log(res);
                  alert("Pokemon Capturado");
                })
                .catch((error) => {
                  alert("Pokemon já foi capturado");
                });
            }}
            icon={faHeart}
          />
          <div className="infos">
            <p className="tipo">{item.kind}</p>
          </div>
          <p className="nome">{item.name}</p>
        </div>
      </div>
    );
  });
  return <div className="grid">{listItemsFav}</div>;
}

export default ListItemsFav;
