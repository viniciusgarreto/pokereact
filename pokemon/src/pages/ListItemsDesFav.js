import "./ListItems.css";
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import { faHeartBroken } from "@fortawesome/free-solid-svg-icons";
// import { navigate } from "@reach/router";

// const pokemonPage = (item) => {
//   var str = item;
//   localStorage.setItem("pokemonAtual", str);
//   navigate("./pokemon");
//   window.location.reload(false);
// };

function ListItemsDesFav(props) {
  var usuario = localStorage.getItem("usuarioAtual");
  console.log("esse é o usuario atual");
  console.log(usuario);
  const items = props.items;
  console.log(props.items);
  const listItemsFav = items.map((item) => {
    return (
      <div className="pokeDisplay" key={item.id}>
        <div
          className="cards2"
        //   onClick={(e) => {
        //     pokemonPage(item.name);
        //   }}
        >
          <img
            src={item.image_url}
            className="foto_pokemon"
            alt="Pokeimage"
          ></img>
          <p className="numero">{item.number}</p>
          <FontAwesomeIcon
            className="faicons"
            onClick={() => {
              axios
                .delete(
                  `https://pokedex20201.herokuapp.com//users/${usuario}/starred/${item.name}`
                )
                .then((res) => {
                  console.log(res);
                  alert("Pokemon Libertado");
                  window.location.reload(false);
                });
            }}
            icon={faHeartBroken}
          />
          <div className="infos">
            <p className="tipo">{item.kind}</p>
          </div>
          <p className="nome">{item.name}</p>
        </div>

        {/* <p>{item.weight}</p>
                <p>{item.height}</p> */}
      </div>
    );
  });
  return <div className="grid">{listItemsFav}</div>;
}

export default ListItemsDesFav;
