import React from "react";
import "./Perfil.css";
import axios from "axios";
import ListPokemon from "./ListPokemon";

var pokemon = localStorage.getItem("pokemonAtual");

console.log(pokemon);
console.log("não entro");

class Pokemon extends React.Component {
  constructor(props) {
    console.log("entrei");
    super(props);

    this.state = {
      items: [],
    };
  }

  componentDidMount() {
    console.log("entrei");
    axios
      .get(`https://pokedex20201.herokuapp.com//pokemons/${pokemon}`)
      .then((res) => {
        this.setState({ items: res.data });
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
    console.log("entro na didmount");
  }

  render() {
    console.log("entro na render");
    return (
      <>
        <p className="captura">Quem é este pokemon?</p>
        <ListPokemon items={this.state.items} />
      </>
    );
  }
}

export default Pokemon;
