import "./Usuario.css";
import React from "react";
import { Link } from "@reach/router";
import axios from "axios";
import ListItemsFav from "./ListItemsFav";

let count = 1;

class Usuario extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
    };
    this.changeNextPage = this.changeNextPage.bind(this);
    this.changePreviousPage = this.changePreviousPage.bind(this);
  }

  componentDidMount() {
    axios
      .get(`https://pokedex20201.herokuapp.com/pokemons?page=1`)
      .then((res) => {
        this.setState({ items: res.data.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  changeNextPage() {
    if (count <= 32) {
      count = count + 1;
    }

    axios
      .get(`https://pokedex20201.herokuapp.com/pokemons?page=${count}`)
      .then((res) => {
        this.setState({ items: res.data.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  changePreviousPage() {
    if (count > 1) {
      count = count - 1;
    }

    axios
      .get(`https://pokedex20201.herokuapp.com/pokemons?page=${count}`)
      .then((res) => {
        this.setState({ items: res.data.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    return (
      <>
        <div className="header_user">
          <Link to="./perfil" className="perfil_b">
            Capturados
          </Link>
          <Link to="/" className="perfil_b">
            Sair
          </Link>
          <img
            className="pokeplus"
            alt="Titulo da Pagina"
            src="https://cdn4.iconfinder.com/data/icons/pokemon-go-5/100/3-512.png"
          />
        </div>

        <p className="captura">Capture seus Pokemons!</p>
        <ListItemsFav className="cards" items={this.state.items} />

        <div className="buttons2">
          <button onClick={this.changePreviousPage} className="antes">
            ⤶
          </button>
          <button onClick={this.changeNextPage} className="depois">
            ⤷
          </button>
        </div>
      </>
    );
  }
}

export default Usuario;
