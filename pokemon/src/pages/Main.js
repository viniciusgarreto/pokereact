import React from "react";
import { Router } from "@reach/router";
import Home from "./Home";
import Login from "./Login";
import Usuario from "./Usuario";
import Perfil from "./Perfil";
import Pokemon from "./Pokemon";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faHeart } from "@fortawesome/free-regular-svg-icons";
import { faHeartBroken } from "@fortawesome/free-solid-svg-icons";

library.add(faHeart);
library.add(faHeartBroken);

const Main = () => (
  <>
    <Router>
      <Home path="/" />
      <Login path="/login" />
      <Usuario path="/usuario" />
      <Perfil path="/usuario/perfil" />
      <Pokemon path="/pokemon" />
    </Router>
  </>
);

export default Main;
