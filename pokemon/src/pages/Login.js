import "./Login.css";
import React from "react";
import axios from "axios";
import { navigate } from "@reach/router";

const Login = () => {
  const input = React.useRef();
  const input2 = React.useRef();

  const send = () => {
    var str = input.current.value;
    localStorage.setItem("usuarioAtual", str);
    axios
      .post("https://pokedex20201.herokuapp.com/users", {
        username: input.current.value,
      })
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
        alert("Usuário já existente");
        navigate("./login");
      })
      .then(navigate("./usuario"));
  };

  const send2 = () => {
    var str = input2.current.value;
    localStorage.setItem("usuarioAtual", str);
    axios
      .post("https://pokedex20201.herokuapp.com/users", {
        username: input2.current.value,
      })
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        alert("Usuario válido");
        navigate("./usuario");
      });
  };

  return (
    <>
      <div className="paginalogin">
        <div className="form">
          <h1>Cadastre-se!</h1>
          <input ref={input} placeholder="Usuário" className="input"></input>
          <button onClick={send} className="login">
            ✓
          </button>
          <h1>Ja possui conta?</h1>
          <input ref={input2} placeholder="Usuário" className="input"></input>
          <button onClick={send2} className="login">
            ✓
          </button>
        </div>
      </div>
    </>
  );
};

export default Login;
