import React from "react";
import { Link } from "@reach/router";
import "./Perfil.css";
import axios from "axios";
import ListItemsDesFav from "./ListItemsDesFav";

var usuario = localStorage.getItem("usuarioAtual");

class Perfil extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
    };
  }

  componentDidMount() {
    axios
      .get(`https://pokedex20201.herokuapp.com/users/${usuario}`)
      .then((res) => {
        this.setState({ items: res.data.pokemons });
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    return (
      <>
        <div className="header_user2">
          <Link to="/usuario" className="perfil_b">
            Continuar Capturando
          </Link>
          <img
            className="backpack"
            alt="Titulo da Pagina"
            src="https://lh3.googleusercontent.com/fHu6q4s7oyapYxcXHbT9r8SztUqZBwyyDXEo5RGC5JxSi7ahSzEuagl0IHOpnRiuVG4=s180"
          />
        </div>
        <p className="captura">Pokemons Capturados</p>
        <ListItemsDesFav items={this.state.items} />
      </>
    );
  }
}

export default Perfil;
