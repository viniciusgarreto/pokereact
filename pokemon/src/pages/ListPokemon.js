import React from "react";
import "./ListItems.css";
import "./ListPokemon.css";

function ListPokemon(props) {
  const items = props.items;
  console.log("oi");
  console.log(items.name);
  return (
    <div className="pokeDisplay2">
      <img src={items.image_url} className="foto" alt="Pokeimage"></img>
      <div className="inform">
        <p className="nome1"> {items.name}</p>
        <div className="xxx">
          <p className="numero1">{items.number}</p>
          <p className="peso">Peso:{items.weight/10}kg</p>
          <p className="tamanho">Tamanho:{items.height/10}m</p>
          <p className="tipo1">Tipo:{items.kind}</p>
        </div>
      </div>
    </div>
  );
}

export default ListPokemon;
