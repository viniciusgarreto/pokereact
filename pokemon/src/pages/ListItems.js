import React from "react";
import "./ListItems.css";
import { navigate } from "@reach/router";

const pokemonPage = (item) => {
  var str = item;
  localStorage.setItem("pokemonAtual", str);
  navigate("./pokemon");
  window.location.reload(false);
};

function ListItems(props) {
  const items = props.items;
  const listItems = items.map((item) => {
    return (
      <div className="pokeDisplay" key={item.id}>
        <div
          className="cards"
          onClick={(e) => {
            pokemonPage(item.name);
          }}
        >
          <img
            src={item.image_url}
            className="foto_pokemon"
            alt="Pokeimage"
          ></img>
          <p className="numero">{item.number}</p>
          <div className="infos">
            <p className="tipo">Tipo:{item.kind}</p>
          </div>
          <p className="nome"> {item.name}</p>
        </div>
      </div>
    );
  });
  return <div className="grid">{listItems}</div>;
}

export default ListItems;
