import React from "react";
import { Link } from "@reach/router";
import axios from "axios";
import ListItems from "./ListItems";
import "./Home.css";

let count = 1;

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
    };
    this.changeNextPage = this.changeNextPage.bind(this);
    this.changePreviousPage = this.changePreviousPage.bind(this);
  }

  componentDidMount() {
    axios
      .get(`https://pokedex20201.herokuapp.com/pokemons?page=1`)
      .then((res) => {
        this.setState({ items: res.data.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  changeNextPage() {
    if (count <= 32) {
      count = count + 1;
    }

    axios
      .get(`https://pokedex20201.herokuapp.com/pokemons?page=${count}`)
      .then((res) => {
        this.setState({ items: res.data.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  changePreviousPage() {
    if (count > 1) {
      count = count - 1;
    }

    axios
      .get(`https://pokedex20201.herokuapp.com/pokemons?page=${count}`)
      .then((res) => {
        this.setState({ items: res.data.data });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    return (
      <>
        <div className="head">
          <img
            className="Titulo"
            alt="Titulo da Pagina"
            src="https://logodownload.org/wp-content/uploads/2017/08/pokemon-logo.png"
          />

          <img
            className="nintendo"
            src="https://logodownload.org/wp-content/uploads/2017/04/nintendo-logo.png"
            alt="Nintendo logo"
          />
          <div className="central">
            <p className="t1">
              Explore nosso
              <br /> catálogo de <br /> Pokemons
            </p>
            <img
              className="ash"
              src="https://www.freepngimg.com/thumb/pokemon/35997-1-anime-pokemon-transparent-picture.png"
              alt="Imagem Ash"
            />
          </div>
        </div>

        <img
          className="pokebola"
          src="https://cdn4.iconfinder.com/data/icons/pokemon-go-5/100/4-512.png"
          alt="Imagem de pokebola"
        />
        <ListItems items={this.state.items} className="pokemons" />
        <div className="buttons">
          <button onClick={this.changePreviousPage} className="antes">
            ⤶
          </button>
          <button onClick={this.changeNextPage} className="depois">
            ⤷
          </button>
        </div>
        <Link className="Login" to="/login">
          LOGIN
        </Link>
      </>
    );
  }
}

export default Home;
